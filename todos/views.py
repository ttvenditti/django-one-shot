from django.shortcuts import render
from django.shortcuts import redirect, render, get_object_or_404
from todos.models import TodoList, TodoItem
from todos.forms import NewListForm, NewItemForm


# Create your views here.


def todo_list(request):
    todo = TodoList.objects.all()
    context = {
        "todo_list": todo,
    }
    return render(request, "todos/lists.html", context)

def todo_item(request, id):
    todoitem = get_object_or_404(TodoList, id=id)
    context = {
        "todoitem": todoitem,
    }
    return render(request, "todos/detail.html", context)

def create_list(request):
    if request.method == "POST":
        form = NewListForm(request.POST)
        if form.is_valid():
            its=form.save()
            return redirect("todo_list_detail", id=its.id)
    else:
        form = NewListForm()
    context = {
        "form": form,
    }
    return render(request, "todos/create.html", context)



def edit_list(request, id):
    its = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = NewListForm(request.POST, instance = its)
        if form.is_valid():
            form.save()

            return redirect("todo_list_detail", id=id)
    else:
        form = NewListForm(instance=its)

    context = {
        "todoitem": its,
        "form": form,
        }
    return render(request, "todos/edit.html", context)

def delete_list(request, id):
  its = TodoList.objects.get(id=id)
  if request.method == "POST":
    its.delete()
    return redirect("todo_list_list")

  return render(request, "todos/delete.html")



def create_item(request):
    if request.method == "POST":
        form = NewItemForm(request.POST)
        if form.is_valid():
            form.save()
        return redirect("todo_list_detail", id=its.list.id)
    else:
        form = NewItemForm()
    context = {
        "form": form,
    }
    return render(request, "todos/itemcreate.html", context)
