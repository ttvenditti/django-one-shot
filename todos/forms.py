from django.forms import ModelForm
from todos.models import TodoList, TodoItem

class NewListForm(ModelForm):    # Step 1
    class Meta:                 # Step 2
        model = TodoList          # Step 3
        fields = [              # Step 4
            "name",            # Step 4

            ]

class NewItemForm(ModelForm):    # Step 1
    class Meta:                 # Step 2
        model = TodoItem          # Step 3
        fields = [              # Step 4
            "task",            # Step 4
]
