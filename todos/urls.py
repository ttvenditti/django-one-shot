from django.urls import include, path
from todos.views import todo_list, todo_item, create_list, edit_list, delete_list, create_item

urlpatterns = [
    path("", todo_list, name = "todo_list_list"),
    path("<int:id>/", todo_item, name = "todo_list_detail"),
    path("create/", create_list, name = "todo_list_create"),
    path("<int:id>/edit/", edit_list, name = "todo_list_update"),
    path("<int:id>/delete/", delete_list, name = "todo_list_delete"),
    path("items/create/", create_item, name = "todo_item_create"),
    ]
